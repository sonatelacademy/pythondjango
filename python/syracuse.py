# Retourne n/2 si n est pair, et 3n+1 si n est impair.
def syracuse(n):
    return

# Affiche, sans rien retourner, la suite de Syracuse partant de n.
def suite_syracuse(n):
    return

# Retourne la longueur de la suite de Syracuse partant de n.
def longueur_syracuse(n):
    longueur = 1
    return longueur

# Affiche pour chaque entier k entre 1 et n, la valeur de k et la longueur de la suite partant de k.
def longueur_syracuse_jusqua(n):
    return

# Retourne le premier entier entre 1 et n pour lequel la longueur de la suite
# de Syracuse est la plus grande, et la longueur de cette suite.
def indice_et_longueur_maximale(n):
    return  indice_longueur_max, longueur_max

print(indice_et_longueur_maximale((20)))            
