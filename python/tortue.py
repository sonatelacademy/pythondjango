from turtle import *
from random import *

def carre(taille):
    return

def polygone(taille, cotes):
    return

def triangle(taille):
    # Utiliser la fonction polygone !
    return

def rosace(taille, cote):


def rosace_coloree(taille, cote):
    # On définit la couleur de départ: rouge
    (rd,gd,bd) = (255, 0, 0)
    # Et la couleur d'arrivée: bleu
    (ra,ga,ba) = (0, 0, 255)
    return

Screen()

colormode(255)

# A partir d'ici, ce sont les tests.
up()
setpos(100, 100)
down()

carre(50)

# reset()

# up()
# setpos(0,0)
# down()

# polygone(50, 5)

# reset()

# up()
# setpos(200,0)
# down()

# setheading(randint(0,360))
# triangle(60)

# reset()

# up()
# setpos(-200, 100)
# down()

# color('red', 'yellow')
# begin_fill()
# speed('fastest')
# polygone(2, 360)
# speed('normal')
# end_fill()


# up()
# setpos(200, 0)
# down()

# pencolor(255, 0, 0)

# speed('fastest')
# rosace(100, 5)
# speed('normal')

# reset()

# speed('fastest')
rosace_coloree(100, 5)
# speed('normal')
