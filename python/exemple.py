def sep():
  print("------------")

print ("Hello !")

sep()
a = 5

print ("a, a*a")
print (a,  a*a)
print ("a vaut", a, "et a*a vaut",  a*a)

sep()
if a*a == a**a:
  print("OK")

sep()
if a*a == a**a:
  print("OK")
else:
  print("KO")

sep()
def f():
  print (2)

a = f()
print ("a = ", a)

sep()
def g():
  return 2

b = g()
print ("b = ", b)

sep()
for n in range(5):
  print (n)

sep()
for n in range(10, 100, 5):
  print (n)

