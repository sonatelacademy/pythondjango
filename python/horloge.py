from turtle import *
from datetime import datetime

def deplacement(distance, angle=0):
	penup()
	right(angle)
	forward(distance)
	left(angle)
	pendown()

def menu(taille, valeur):
	forward(taille*1.15)
	right(90)
	forward(valeur/2.0)
	left(120)
	forward(valeur)
	left(120)
	forward(valeur)
	left(120)
	forward(valeur/2.0)

def timer_hands(name, taille, valeur):
	reset()
	deplacement(-taille*0.15)
	begin_poly()
	menu(taille, valeur)
	end_poly()
	clock_labellings = get_poly()
	register_shape(name, clock_labellings)

def face(degre):
	reset()
	pensize(7)
	for x in range(60):
		deplacement(degre)
		if x % 5 == 0:
			forward(25)
			deplacement(-degre-25)
		else:
			dot(3)
			deplacement(-degre)
		rt(6)

def parametre():
	global second_hand, minute_hand, hour_hand
	timer_hands("second_hand", 125, 25)
	timer_hands("minute_hand", 130, 25)
	timer_hands("hour_hand", 90, 25)
	face(160)
	second_hand = Turtle()
	second_hand.shape("second_hand")
	second_hand.color("gray40", "black")
	minute_hand = Turtle()
	minute_hand.shape("minute_hand")
	minute_hand.color("red", "orange")
	hour_hand = Turtle()
	hour_hand.shape("hour_hand")
	hour_hand.color("black", "gray")
	for hand in second_hand, minute_hand, hour_hand:
		hand.resizemode("user")
		hand.shapesize(1,1,3)
		hand.speed(0)
	ht()

def tick():
	t = datetime.today()
	secondTimer = t.second + t.microsecond*0.000001
	minute = t.minute + secondTimer/60.0
	onTheHour = t.hour + minute/60.0
	try:
		tracer(False)
		second_hand.setheading(6*secondTimer)
		minute_hand.setheading(6*minute)
		hour_hand.setheading(30*onTheHour)
		tracer(True)
		ontimer(tick, 100)
	except Terminator:
		pass

def main():
	tracer(False)
	bgcolor("white")
	parametre()
	tracer(True)
	tick()

	return "FAIT"

if __name__ == "__main__":
	mode("logo")
	msg = main()
	print(msg)
	mainloop()

