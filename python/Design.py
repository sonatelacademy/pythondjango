import turtle;
trtl = turtle.Turtle()
turtle.bgcolor("Black") # set background color
trtl.speed(0) # by default speed
turtle.title("Design") # title of your Design
def drawRainbow():
	for i in range(500):
		trtl.color("#F98BB4") #color of pointer/pen
		trtl.backward(i) #go backward side with growing in length
		trtl.left(110) #turn 110 degre left
	trtl.color("White") # select turtle color
	trtl.hideturtle() # make turtle invisible
	trtl.setpos((20,0)) # set position x=20, y=0
drawRainbow()